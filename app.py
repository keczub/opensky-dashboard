from libs.logic_library import User, Plots, Interval, geocode_text
import plotly.graph_objects as go

# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State

app = dash.Dash(__name__)
server = app.server

initial_empty_plots = Plots()
initial_empty_radar_fig = initial_empty_plots.createRadar(None)
initial_empty_table_fig = initial_empty_plots.createTable(None)
initial_empty_map_fig = initial_empty_plots.draw_map(None, None)
initial_empty_bar_fig = Interval.drawBarPlot(None, None)

app.layout = html.Div(id="main-div",
                      children=[html.Div(id="header-div",
                                         children=[html.H2(children="What plane is now flying above me ?",
                                                           style={'marginLeft': 10,
                                                                  'font-size': 20,
                                                                  'font-family': 'verdana',
                                                                  'font-style': 'normal',
                                                                  'font-weight': 'bold',
                                                                  "display": "inline-block"}),
                                                   html.H2(children=" - OpenSky & Plotly powered plane radar dashboard",
                                                           style={'marginLeft': 2,
                                                                  'font-size': 16,
                                                                  'font-family': 'verdana',
                                                                  'font-style': 'normal',
                                                                  "display": "inline-block"})]),
                                html.Div(id="control-bar-div",
                                         children=[html.Div([html.H6(children="Set up your location: ",
                                                                     style={"display": "inline-block",
                                                                            'marginLeft': 10,
                                                                            'marginRight': 10,
                                                                            'font-size': 14,
                                                                            'font-family': 'verdana',
                                                                            'font-style': 'normal'}),
                                                            dcc.Input(id="location_input",
                                                                      value="London",
                                                                      style={"display": "inline-block"})],
                                                            style={"display": "inline-block"}),
                                                   html.Div([html.H6(children=" Current status: ",
                                                                     style={"display": "inline-block",
                                                                            'marginLeft': 100,
                                                                            'marginRight': 10,
                                                                            'font-size': 14,
                                                                            'font-family': 'verdana',
                                                                            'font-style': 'normal'}),
                                                            dcc.RadioItems(options=[{'label': "Active",
                                                                                     'value': '',
                                                                                     'disabled': True}],
                                                                           value=None,
                                                                           id="radio-btn",
                                                                           style={"display": "inline-block"}),
                                                            html.H6(children="Updating: ",
                                                                    style={"display": "inline-block",
                                                                           'marginLeft': 100,
                                                                           'marginRight': 10,
                                                                           'font-size': 14,
                                                                           'font-family': 'verdana',
                                                                           'font-style': 'normal'}),
                                                            html.Button(children="Stop",
                                                                        id="stop-btn",
                                                                        n_clicks=0,
                                                                        style={"display": "inline-block"})],
                                                   style={"display": "inline-block"}),
                                                   html.H6(id="text_status",
                                                           children="",
                                                           style={'marginLeft': 10,
                                                                  'font-size': 14,
                                                                  'font-family': 'verdana',
                                                                  'font-style': 'normal'})]),
                                html.Div([html.Div(id='map-div',
                                                   children=dcc.Graph(id='map-update-plot',
                                                                      figure={'data': initial_empty_map_fig.data,
                                                                              'layout': initial_empty_map_fig.layout},
                                                                      config={'scrollZoom': False,
                                                                              'displayModeBar': False,
                                                                              'doubleClick': False}),
                                                   className='eight columns'),
                                          html.Div(id='radar-div',
                                                   children=dcc.Graph(id='radar-update-plot',
                                                                      figure={'data': initial_empty_radar_fig.data,
                                                                              'layout': initial_empty_radar_fig.layout}),
                                                   className='four columns')]),
                                html.Div([html.Div(id='table-div',
                                                   children=dcc.Graph(id='table-update-plot',
                                                                      figure={'data': initial_empty_table_fig.data,
                                                                              'layout': initial_empty_table_fig.layout}),
                                                   className='eight columns'),
                                         html.Div(id='bar-div',
                                                  children=dcc.Graph(id='bar-update-plot',
                                                                     figure={'data': initial_empty_bar_fig.data,
                                                                             'layout': initial_empty_bar_fig.layout}),
                                                  className='four columns')]),
                                dcc.Interval(id='interval-component',
                                             interval=15000,
                                             n_intervals=0,
                                             disabled=False),
                                dcc.Interval(id='interval-component-radiobtn',
                                             interval=550,
                                             n_intervals=0,
                                             disabled=False),
                                html.Div(id="geocoded_text",
                                         hidden=True),
                                dcc.Store(id='memory',
                                          data={"number_of_states_list": [0]*20,
                                                "state_time_list": [i for i in range(-20, 0)]})])


# Multiple components can update every time interval gets fired.
@app.callback([Output('radar-update-plot', 'figure'),
               Output('map-update-plot', 'figure'),
               Output('map-update-plot', 'config'),
               Output('table-update-plot', 'figure'),
               Output('bar-update-plot', 'figure'),
               Output('memory', 'data')],
              [Input('interval-component', 'n_intervals'),
               Input('interval-component', "disabled"),
               Input("geocoded_text", "children")],
              [State('memory', 'data')])
def update_graph_live(_, disabled, location_name, data):

    number_of_states_list = data["number_of_states_list"]
    state_time_list = data["state_time_list"]

    current_user = User(float(location_name[0]), float(location_name[1]))
    current_interval = Interval(current_user, number_of_states_list, state_time_list)
    return_data = {"number_of_states_list": number_of_states_list, "state_time_list": state_time_list}

    radar_fig = current_interval.radar
    table_fig = current_interval.table
    map_fig = current_interval.map
    bar_fig = current_interval.bar

    if disabled is False:
        config = {'scrollZoom': False,  'displayModeBar': False, 'doubleClick': False}
    else:
        config = {'scrollZoom': True,  'displayModeBar': False, 'doubleClick': False}

    return radar_fig, map_fig, config, table_fig, bar_fig, return_data


@app.callback([Output("geocoded_text", "children"),
               Output("text_status", "children")],
              [Input('location_input', "value")])
def get_user_input(text):

    location_coords, location_name = geocode_text(text)
    return location_coords, location_name


@app.callback([Output("interval-component", "disabled"),
               Output("stop-btn", "children"),
               Output('interval-component-radiobtn', 'disabled'),
               Output('interval-component-radiobtn', "n_intervals")],
              [Input('stop-btn', "n_clicks")])
def stop_upd(n):

    if (n % 2 != 0) and (n != 0):
        return True, "Start", True, 0
    else:
        return False, "Stop", False, 2


@app.callback(Output("radio-btn", "value"),
              [Input('interval-component-radiobtn', "n_intervals")])
def radio_btn_upd(n: int):

    if n % 2 != 0:
        return ''
    else:
        return None


if __name__ == '__main__':
    app.run_server(debug=True)
