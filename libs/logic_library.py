import requests, os
import plotly.graph_objects as go
from geographiclib.geodesic import Geodesic
import geocoder
from typing import TypeVar, Tuple, List, Union

UserClass = TypeVar('User')
StateClass = TypeVar('State')
PlotlyFigure = TypeVar('plotly.figure')


class User:
    """
    User class that reflects a single person at given location

    :param longitude: user logitude in decimal degrees
    :param latitude: user latitude in decimal degrees

    :ivar rgb_color: unique color for respresenting user in plots
    :type rgb_color: string of rgb values in format "rgba(R, G, B, A)"

    :Example:
        >>> user_1 = User(18.525833, 53.963056)
    """

    def __init__(self, longitude: float, latitude: float):

        self.longitude = longitude
        self.latitude = latitude
        self.rgb_color = "rgba({0},{1},{2},{3})".format(243, 189, 61, 1)


class State:
    """
    State class that reflects a single plane vector state

    :param raw_state: list of state vector attributes
    :param user: User object with known location

    :ivar icao24: Unique ICAO 24-bit address of the transponder in hex string representation.
    :type icao24: string
    :ivar callsign: Callsign of the vehicle (8 chars). Can be null if no callsign has been received.
    :type callsign: string
    :ivar origin_country: Country name inferred from the ICAO 24-bit address.
    :type origin_country: string
    :ivar time_position: Unix timestamp (seconds) for the last position update. Can be null if no position report was
                         received by OpenSky within the past 15s.
    :type time_position: int
    :ivar last_contact: Unix timestamp (seconds) for the last update in general. This field is updated for any new,
                        valid message received from the transponder.
    :type last_contact: int
    :ivar longitude: WGS-84 longitude in decimal degrees. Can be null.
    :type longitude: float
    :ivar latitude: WGS-84 latitude in decimal degrees. Can be null.
    :type latitude: float
    :ivar baro_altitude: Geometric altitude in meters. Can be null.
    :type baro_altitude: float
    :ivar on_ground: Boolean value which indicates if the position was retrieved from a surface position report.
    :type on_ground: boolean
    :ivar velocity: Velocity over ground in m/s. Can be null.
    :type velocity: float
    :ivar heading: True track in decimal degrees clockwise from north (north=0°). Can be null.
    :type heading: float
    :ivar vertical_rate: Vertical rate in m/s. A positive value indicates that the airplane is climbing, a negative
                         value indicates that it descends. Can be null.
    :type vertical_rate: float
    :ivar sensors: IDs of the receivers which contributed to this state vector. Is null if no filtering for sensor was
                   used in the request.
    :type sensors: int[]
    :ivar geo_altitude: Geometric altitude in meters. Can be null.
    :type geo_altitude: float
    :ivar squawk: The transponder code aka Squawk. Can be null.
    :type squawk: string
    :ivar spi: Whether flight status indicates special purpose indicator.
    :type spi: boolean
    :ivar position_source: Origin of this state’s position: 0 = ADS-B, 1 = ASTERIX, 2 = MLAT
    :type position_source: int

    :ivar related_distance: distance from user to plane
    :type related_distance: float
    :ivar related_azimuth: azimtuh from user to plane
    :type related_azimuth: int
    :ivar direction_vector: movement vector to plot it on map
    :type direction_vector: list of coordinates in format [[lat1, long1],[lat2, long2]]
    :ivar rgba_color: unique color for respresenting user in plots
    :type rgba_color: string of rgb values in format "rgba(R, G, B, A)"
    :ivar geod:
    :type geod:

    :Example:

        >>> raw_state = ["a09281", "", "United States", 1591898759, 1591898765, \\
                        -84.7186, 34.2187, 975.36, false, 55.2, 140.67, -1.3, null, \\
                        1074.42, null, false, 0]
        >>> user_1 = User(-84.7000, 34.2187)
        >>> State(raw_state, user_1)
    """
        
    def __init__(self, raw_state: list, user: UserClass):
        
        # Information obtained from response
        self.icao24, self.callsign, self.origin_country, self.time_position,\
        self.last_contact, self.longitude, self.latitude, self.baro_altitude, \
        self.on_ground, self.velocity, self.heading, self.vertical_rate, \
        self.sensors, self.geo_altitude, self.squawk, self.spi, \
        self.position_source = raw_state
        
        # Position related to User
        self.related_distance, self.related_azimuth = None, None

        # Setting a reference system to calculate distance
        self.geod = Geodesic.WGS84

        # Calculate direction vector for visualization
        self.direction_vector = self.calcaulateDirection()

        # Calculate reference values to user position
        self.calcaulate_reference(user)

        # Create rgb color 
        self.rgba_color = "rgba({0},{1},{2},{3})".format(153, 153, 255, 1)

    def calcaulate_reference(self, user: UserClass) -> None:
        """
        Calculates distance and angle between user and plane to plot on radar

        :param user: User instance with defined position
        :return: None
        """
        
        # Calculating distance and angle between user and plane to plot on radar
        reference_dict = self.geod.Inverse(user.latitude, user.longitude, self.latitude, self.longitude)
        if reference_dict['azi1'] > 0:
            self.related_azimuth = reference_dict['azi1']
        else:
            self.related_azimuth = reference_dict['azi1'] + 360
        self.related_distance = reference_dict['s12']
    
    def calcaulateDirection(self) -> Union[None, list]:
        """
        Calculates movement direction of a plane to plot on map

        :return: None or list of coordinates
        """
        try:
            # Calculating direction requires angle modification to set geographic north as a beginning
            # Velocity multiplier helps visualize speed on map
            geod_dict = self.geod.Direct(self.longitude, self.latitude, -self.heading+90, 20000)
            return [[geod_dict['lat1'], geod_dict['lat2']], [geod_dict['lon1'], geod_dict['lon2']]]
        except:
            return None


class Interval:
    """
     Interval class that reflects a plane vector states from one request

     :param user: User object with known location
     :param number_of_states_list: 20 elements length list with number of planes for interval
     :param state_time_list: 20 elements length list with index of interval

     :ivar last_response: List with airplanes states in the form of [[attr1, attr2, ..., attr17], [], [], ...]
     :ivar states_list: list with States object
     :ivar plots: Plots Class instance
     :ivar map:  PlotlyFigure with map
     :ivar table: PlotlyFigure with table
     :ivar radar: PlotlyFigure with radar plot
     :ivar bar: PlotlyFigure with bar plot
    """

    def __init__(self, user: UserClass, number_of_states_list: List[int], state_time_list: List[int]):
        self.last_response = self.make_request(user)
        self.states_list = []
        self.create_states_from_response(user)
        
        # Create plots based only on interval data
        self.plots = Plots()
        self.map = self.plots.draw_map(user, self.states_list)
        self.table = self.plots.createTable(self.states_list)
        self.radar = self.plots.createRadar(self.states_list)
        
        # Plot based on cached values form other intervals
        self.createListsForBarPlot(number_of_states_list, state_time_list)
        self.bar = self.drawBarPlot(number_of_states_list, state_time_list)

    def create_states_from_response(self, user: UserClass) -> None:
        """
        Creates State object from received response.
        :param user: User object with known location
        :return: None
        """

        for raw_state in self.last_response:
            state = State(raw_state, user)
            self.states_list.append(state)


    def createListsForBarPlot(self, number_of_states_list: List[int], state_time_list: List[int]) -> None:
        """
        Updates a values list for Bar Plot.

        :param number_of_states_list: 20 elements length list with number of planes for interval
        :param state_time_list: 20 elements length list with index of interval
        :return: None
        """
    
        # List of 20 last values of aircraft for interval
        
        if len(number_of_states_list) > 20:
            number_of_states_list.pop(0)
            number_of_states_list.append(len([i for i in self.states_list]))
        else:
            number_of_states_list.append(len([i for i in self.states_list]))
        
        if len(number_of_states_list) > 20:
            state_time_list.pop(0)
            state_time_list.append(state_time_list[-1]+1)
        else:
            state_time_list.append(state_time_list[-1]+1)

    def make_request(self, user: UserClass) -> List[list]:
        """
        Make request based on user location. OpenSKy API allows to query a certain area defined by a bounding box
        of WGS84 coordinates.

        :param user: User object with known location
        :return: List with airplanes states in the form of [[attr1, attr2, ..., attr17], [], [], ...]
        """
        
        # Extent multiplier
        d = 2
        
        lamin = user.latitude - 0.5*d
        lomin = user.longitude - 1.0*d
        lamax = user.latitude + 0.5*d
        lomax = user.longitude + 1.0*d
        response = requests.get("https://opensky-network.org/api/states/all?lamin={0}&lomin={1}&lamax={2}&lomax={3}"
                         .format(lamin, lomin, lamax, lomax))
        return response.json()['states']
        
    @staticmethod
    def drawBarPlot(states_list, lista_time) -> PlotlyFigure:

        if (states_list is None) and (lista_time is None):
                # number of aircrafts on map
                fig = go.Figure(data=[go.Bar(name="amount", x=[None], y=[None])])
                fig.update_traces(marker_color='rgb(122, 215, 183)', marker_line_color='rgb(122, 215, 183)',
                                  marker_line_width=1.5, opacity=0.8)
                fig.update_layout(margin=dict(l=30, r=30, t=100, b=100))
                fig.update_xaxes(title_text='N-th request')
                fig.update_yaxes(title_text='Number of Aircraft states <br> in request response',
                                 title_font=dict(size=14))
        else:
                # number of aircrafts on map
                fig = go.Figure(data=[go.Bar(name="amount", x=lista_time, y=states_list)])
                fig.update_traces(marker_color='rgb(122, 215, 183)', marker_line_color='rgb(122, 215, 183)',
                                  marker_line_width=1.5, opacity=0.8)
                fig.update_layout(margin=dict(l=30, r=30, t=100, b=100))
                fig.update_xaxes(title_text='N-th request')
                fig.update_yaxes(title_text='Number of Aircraft states <br> in request response',
                                 title_font=dict(size=14))
      
        return fig
 
class Plots:
    def __init__(self):
        # Plots general variables
        self.mapbox_access_token = os.environ['MAPBOX_TOKEN']
        #self.mapbox_access_token = open('.mapbox_token').read()
        self.first_distance_threshold = 50000
        self.first_rgb_color = "rgba({0},{1},{2},{3})".format(132, 73, 174, 1)
        self.second_distance_threshold = 150000
        self.second_rgb_color = "rgba({0},{1},{2},{3})".format(83, 133, 72, 1)
              
    def draw_map(self, user, states_list) -> PlotlyFigure:
        fig = go.Figure()
        geod = Geodesic.WGS84
        if (user is None) and (states_list is None):
            fig.add_trace(go.Scattermapbox(
                    lat=[None],
                    lon=[None],
                    mode='markers',
                    name="User",
                    marker=go.scattermapbox.Marker(
                        size=14,
                        color="rgba({0},{1},{2},{3})".format(243, 189, 61, 1)),
                    text=['User']))

            fig.add_trace(go.Scattermapbox(
                    lat=[None],
                    lon=[None],
                    mode="markers",
                    name="50 km",
                    marker=go.scattermapbox.Marker(
                        size=5,
                        color=self.first_rgb_color),
                    hoverinfo='none'))

            fig.add_trace(go.Scattermapbox(
                    lat=[None],
                    lon=[None],
                    mode="markers",
                    name="50 km",
                    marker=go.scattermapbox.Marker(
                        size=5,
                        color= self.second_rgb_color),
                    hoverinfo='none'))

            fig.add_trace(go.Scattermapbox(
                        lat=[None],
                        lon=[None],
                        mode="lines",
                        legendgroup="group1",
                        name='Aircraft directions',
                        marker=go.scattermapbox.Marker(
                            size=10,
                            color="rgba({0},{1},{2},{3})".format(153, 153, 255, 1)),
                        hoverinfo='none'))

            fig.add_trace(go.Scattermapbox(
                        lat=[None],
                        lon=[None],
                        mode='markers',
                        legendgroup="group2",
                        name='Aircraft positions',
                        marker=go.scattermapbox.Marker(
                            size=10,
                            color="rgba({0},{1},{2},{3})".format(153, 153, 255, 1)),
                        hoverinfo='text'))

            fig.update_layout(
                height=600,
                margin=dict(l=20*2, r=20*2, t=0, b=0),
                hovermode='closest',
                legend_orientation="h",
                mapbox=dict(
                    accesstoken=self.mapbox_access_token,
                    bearing=0,
                    center=go.layout.mapbox.Center(
                        lat=0,
                        lon=0),
                    pitch=0,
                    zoom=4))

        else:
            fig.add_trace(go.Scattermapbox(
                    lat=[user.latitude],
                    lon=[user.longitude],
                    mode='markers',
                    name="User",
                    marker=go.scattermapbox.Marker(
                        size=14,
                        color= user.rgb_color),
                    text=['User']))

            fig.add_trace(go.Scattermapbox(
                    lat=[j['lat2'] for j in [geod.Direct(user.latitude, user.longitude, i, self.first_distance_threshold) for i in list(range(0, 361, 15))]],
                    lon=[j['lon2'] for j in [geod.Direct(user.latitude, user.longitude, i, self.first_distance_threshold) for i in list(range(0, 361, 15))]],
                    mode="markers",
                    name="50 km",
                    marker=go.scattermapbox.Marker(
                        size=5,
                        color=self.first_rgb_color),
                    hoverinfo='none'))

            fig.add_trace(go.Scattermapbox(
                    lat=[j['lat2'] for j in [geod.Direct(user.latitude, user.longitude, i, self.second_distance_threshold) for i in list(range(0, 361, 5))]],
                    lon=[j['lon2'] for j in [geod.Direct(user.latitude, user.longitude, i, self.second_distance_threshold) for i in list(range(0, 361, 5))]],
                    mode="markers",
                    name="150 km",
                    marker=go.scattermapbox.Marker(
                        size=5,
                        color=self.second_rgb_color),
                    hoverinfo='none'))

            for number, state in enumerate(states_list):
                if number == 0:
                    fig.add_trace(go.Scattermapbox(
                        lat=state.direction_vector[1],
                        lon=state.direction_vector[0],
                        mode="lines",
                        legendgroup="group1",
                        name='Aircraft directions',
                        marker=go.scattermapbox.Marker(
                            size=10,
                            color=state.rgba_color),
                        hoverinfo='none'))

                    fig.add_trace(go.Scattermapbox(
                        lat=[state.latitude],
                        lon=[state.longitude],
                        mode='markers',
                        legendgroup="group2",
                        name='Aircraft positions',
                        marker=go.scattermapbox.Marker(
                            size=10,
                            color=state.rgba_color
                        ),
                        hoverinfo='text',
                        text=state.callsign,
                    ))
                else:
                    fig.add_trace(go.Scattermapbox(
                        lat=state.direction_vector[1],
                        lon=state.direction_vector[0],
                        mode="lines",
                        legendgroup="group1",
                        marker=go.scattermapbox.Marker(
                            size=10,
                            color=state.rgba_color
                        ),
                        hoverinfo='none',
                        showlegend=False
                    ))

                    fig.add_trace(go.Scattermapbox(
                        lat=[state.latitude],
                        lon=[state.longitude],
                        mode='markers',
                        legendgroup="group2",
                        marker=go.scattermapbox.Marker(
                            size=10,
                            color=state.rgba_color
                        ),
                        hoverinfo='text',
                        text=state.callsign,
                        showlegend=False
                    ))

            fig.update_layout(
                height=600,
                margin=dict(l=40, r=40, t=0, b=0),
                hovermode='closest',
                legend_orientation="h",
                mapbox=dict(
                    accesstoken=self.mapbox_access_token,
                    bearing=0,
                    center=go.layout.mapbox.Center(
                        lat=user.latitude,
                        lon=user.longitude),
                    pitch=0,
                    zoom=6))

        return fig
               
    def createTable(self, states_list) -> PlotlyFigure:
        fig = go.Figure()
        if states_list is None:
            fig.add_trace(go.Table(
                                header=dict(values=[['Callsign'], ['icao24'], ['Origin country'], ['Geo altitude'],['On ground'],
                                                    ['Squawk'],['Velocity']],
                            fill_color="rgb(122, 215, 183, 1)",
                            align='center',
                            line_color='darkslategray',
                            font=dict(color='black', size=13))))
        else:
            fig.add_trace(go.Table(
                header=dict(values=[['Callsign'], ['icao24'], ['Origin country'], ['Geo altitude'],['On ground'],
                                    ['Squawk'],['Velocity']],
                            fill_color="rgb(122, 215, 183, 1)",
                            align='center',
                            line_color='darkslategray',
                            font=dict(color='black', size=13)),
                cells=dict(values=[[i.callsign for i in states_list],
                                     [i.icao24 for i in states_list],
                                     [i.origin_country for i in states_list],
                                     [i.geo_altitude for i in states_list],
                                     [i.on_ground for i in states_list],
                                     [i.squawk for i in states_list],
                                     [i.velocity for i in states_list]],
                           fill_color=["rgb(122, 215, 183, 1)",'white'],
                           line_color=['darkslategray','black'],
                           font=dict(color=['black', 'black'], size=12),
                           align='center')))

        return fig
        
    def createRadar(self, states_list) -> PlotlyFigure:

        # creates a empty figure
        fig = go.Figure()

        # adding a user trace in the center of radar
        fig.add_trace(go.Scatterpolar(
                        r = [0],
                        theta = [0],
                                mode = 'markers',
                                name="User",
                                marker_color = "rgba({0},{1},{2},{3})".format(243, 189, 61, 1),
                                hoverinfo='none'))


        if states_list is None:
                fig.add_trace(go.Scatterpolar(
                                r = [self.first_distance_threshold/1000]*len((range(0, 361, 15))),
                                theta = list(range(0, 361, 15)),
                                mode = 'lines',
                                name="50 km",
                                line_dash = "dash",
                                line_color = self.first_rgb_color,
                                hoverinfo='none'))

                fig.add_trace(go.Scatterpolar(
                                r = [self.second_distance_threshold/1000]*len((range(0, 361, 15))),
                                theta = list(range(0, 361, 15)),
                                mode = 'lines',
                                name="150 km",
                                line_dash = "dash",
                                line_color = self.second_rgb_color,
                                hoverinfo='none'))


                fig.add_trace(go.Scatterpolar(r = [None],
                                        theta = [None],
                                        mode = 'markers',
                                        marker_color="rgba({0},{1},{2},{3})".format(153, 153, 255, 1),
                                        name="Aircraft",
                                        showlegend=True))

                fig.update_layout(margin=dict(l=40, r=40, t=40, b=40),
                             polar = dict(
                             radialaxis_angle = 90,
                             angularaxis = dict(
                             direction = "clockwise")))
        else:
        
                fig.add_trace(go.Scatterpolar(
                            r = [self.first_distance_threshold/1000]*len((range(0, 361, 15))),
                            theta = list(range(0, 361, 15)),
                            mode = 'lines',
                            name="50 km",
                            line_dash = "dash",
                            line_color = self.first_rgb_color,
                            hoverinfo='none'))

                fig.add_trace(go.Scatterpolar(
                            r = [self.second_distance_threshold/1000]*len((range(0, 361, 15))),
                            theta = list(range(0, 361, 15)),
                            mode = 'lines',
                            name="150 km",
                            line_dash = "dash",
                            line_color = self.second_rgb_color,
                            hoverinfo='none'))

                for state in states_list:
                    # related distance in meters, changed to km
                    if state.related_distance/1000 < self.second_distance_threshold/1000:
                            fig.add_trace(go.Scatterpolar(r = [round(state.related_distance/1000, 2)],
                                    theta = [state.related_azimuth],
                                    mode = 'markers',
                                    marker_color=state.rgba_color,
                                    name=state.callsign,
                                    showlegend=True))
                                
        fig.update_layout(margin=dict(l=40, r=40, t=40, b=40),
                         polar = dict(
                         radialaxis_angle = 90,
                         angularaxis = dict(
                         direction = "clockwise")))
                         
        return fig


def geocode_text(text: str) -> Tuple[Tuple[float, float], str]:
    """Function geocode text to coordinates

    :param text: User typed location
    :return: coordinates and textual address

    :Example:
        >>> geocode_text("Gdansk")
        ((18.6452324, 54.347629), 'Location set to: Gdańsk, województwo pomorskie, Polska')
        >>> geocode_text(">?>")
        ((0.0, 0.0), 'Geocoding for entered location failed.')
    """
    response = geocoder.osm(text)
    if response.status_code == 200:
        try:
            lng = response.json['lng']
            lat = response.json['lat']
            address = response.current_result.address
            return (lng, lat), "Location set to: {0}".format(address)
        except:
            return (0.0, 0.0), "Geocoding for entered location failed."
    else:
        return (0.0, 0.0), "Geocoder status code: {0}".format(response.status_code)
